import processing.pdf.*;
int num = 2000;
int range = 5;

int prevX = 0;
//colors
int r = 255;
int g = 255;
int b = 255;

boolean changed = false;
boolean record;


float[] ax = new float[num];
float[] ay = new float[num]; 



void setup() 
{
  size(1980, 1400);
  for(int i = 0; i < num; i++) {
    ax[i] = width/2;
    ay[i] = height/2;
  }
  frameRate(30);
}

void draw() 
{
  if (record) {
    // Note that #### will be replaced with the frame number. Fancy!
    beginRecord(PDF, "perturbation-####.pdf"); 
  }
  
  background(0);
  
  // Shift all elements 1 place to the left
  for(int i = 1; i < num; i++) {
    ax[i-1] = ax[i];
    ay[i-1] = ay[i];
  }

  // Put a new value at the end of the array
  ax[num-1] += random(-range, range);
  ay[num-1] += random(-range, range);
 
  // Constrain all points to the screen
  ax[num-1] = constrain(ax[num-1], 0, width);
  ay[num-1] = constrain(ay[num-1], 0, height);
  
  // Draw a line connecting the points
  for(int i=1; i<num; i++) {    
    float val = float(i)/num * 204.0 + 51;
    if (changed){
      stroke(r,g,b);
    } else {
      stroke(val);
    }
    line(ax[i-1], ay[i-1], ax[i], ay[i]);
  }
}

void mouseMoved() {
  //value = value + 5;
  //if (value > 255) {
  //  value = 0;
  //}
  int mouseChange = abs(prevX-mouseX);
  
  if (mouseChange <= 3 && mouseChange >0) {
    changed = true;
    r = 200;
    g = 200;
    b = 33;
    range = 20;
  }
  else if (mouseChange <= 10 && mouseChange > 3){
    changed = true;
    r = 200;
    g = 120;
    b = 10;
    range = 120;
  }
  else if (mouseChange > 10){
    changed = true;
    r = 255;
    g = 10;
    b = 10;
    range = 200;
  }
  else {
    changed = false;
    range = 5;
  }
    
  println(mouseChange);
  
  
  prevX = mouseX;
}

void keyPressed() {
  record = true;
} 

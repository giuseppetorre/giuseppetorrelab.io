﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class TextControls : MonoBehaviour
{
    public TextMeshProUGUI textObject;
    public int deathType;
    public int suicideRoom;
    public int murderRoom;
    public int anomolyRoom;
    public bool canRestart;
    string room;

    // Start is called before the first frame update
    void Start()
    {
       
        deathType = Random.Range(1, 3);
        causeOfDeath();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && canRestart == true)
        {
            SceneManager.LoadScene("maingame");
        }
    }

    public void causeOfDeath()
    {       
        Debug.Log(deathType);

        if (deathType == 1)
        {
            suicide();
        }
        else if (deathType == 2)
        {
            murder();
        }
        else if (deathType == 3)
        {
            //anomoly();
        }

    }
    #region cause of death
    public void suicide()
    {
        
        suicideRoom = Random.Range(1, 5);
        Debug.Log(suicideRoom);
        if (suicideRoom == 1)
        {
            MasterBedroom();
        }
        else if (suicideRoom == 2)
        {
            Kitchen();
        }
        else if (suicideRoom == 3)
        {
            study();
        }
        else if (suicideRoom == 4)
        {
            Bathroom();
        }
    }
   
    public void murder()
    {
        murderRoom = Random.Range(1, 5);
        Debug.Log(murderRoom);
        if (murderRoom == 1)
        {
            MasterBedroom();
        }
        else if (murderRoom == 2)
        {
            Kitchen();
        }
        else if (murderRoom == 3)
        {
            Hallway();
        }
        else if (murderRoom == 4)
        {
            DiningRoom();
        }
    }
    /*
    public void anomoly()
    {
        anomolyRoom = Random.Range(1, 5);
        if (anomolyRoom == 1)
        {
            MasterBedroom();
        }
        else if (anomolyRoom == 2)
        {
            Bathroom();
        }
        else if (anomolyRoom == 3)
        {
            Hallway();
        }
        else if (anomolyRoom == 4)
        {
            LivingRoom();
        }
    }*/
    #endregion

    #region rooms
    void MasterBedroom()
    {
        room = "<#f7790a>The death occured in the Master Bedroom";
        DisplayText();
    }
    void Kitchen()
    {
        room = "<#f7790a>The death occured in the Kitchen";
        DisplayText();
    }
    void study()
    {
        room = "<#f7790a>The death occured in the Study";
        DisplayText();
    }
    void Bathroom()
    {
        room = "<#f7790a>The death occured in the Bathroom";
        DisplayText();
    }
    void Hallway()
    {
        room = "<#f7790a>The death occured in the Hallway";
        DisplayText();
    }
    void DiningRoom()
    {
        room = "<#f7790a>The death occured in the Dining Room";
        DisplayText();
    }
    void LivingRoom()
    {
        room = "<#f7790a>The death occured in the Living Room";
        DisplayText();
    }
    #endregion

    void DisplayText()
    {
        textObject = gameObject.GetComponent<TextMeshProUGUI>();

        textObject.text = "<size=36>welcome to the manor!</align>" + "\n<size=28>A death has occured and you are the investigator tasked with proving the suspects innocence or guilt by determining if the death was a <#d90d0d>murder "
           + "or suicide <#ffffff>" + "\n"+ room + "\n<#ffffff>please press a direction to begin";
    }
}

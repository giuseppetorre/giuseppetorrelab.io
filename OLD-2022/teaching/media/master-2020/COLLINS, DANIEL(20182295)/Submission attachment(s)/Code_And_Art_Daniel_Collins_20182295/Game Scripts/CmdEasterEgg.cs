﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CmdEasterEgg : MonoBehaviour
{
   // public string inputText;
    public TMP_InputField inputField;
    public TextScroll typeOutTextFR;
    public TextScroll typeOutTextDN;
    public TextScroll typeOutTextGF;
    public string answer1 = "file.restore";
    public string answer2 = "dev.narnia";
    public string answer3 = "game.fin";

    SpriteRenderer sr;
    public GameObject cmdText;
    public GameObject generalText;
    public bool cmdOn;

    public InputController playerController;
    public textBlocks textblock;

     void Start()
    {
        sr = this.gameObject.GetComponent<SpriteRenderer>();
        cmdOn = false;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.C) && Input.GetKey(KeyCode.M))
        {
           
            cmdText.SetActive(true);
            generalText.SetActive(false);
            sr.enabled = true;
            cmdOn = true;
        }
        
        if (Input.GetKeyUp(KeyCode.KeypadEnter) && cmdOn == true)
        {           
            testString(inputField.text);
        }
        //inputText = inputField.GetComponent<TMP_InputField>().text;

        if (Input.GetKey(KeyCode.Escape))
        {
            cmdText.SetActive(false);
            generalText.SetActive(true);
            sr.enabled = false;
            cmdOn = false;        
        }
    }

    public void testString(string inputField)
    {
        if (inputField == answer1)
        {
            typeOutTextFR.enabled = true;
            typeOutTextFR.start = true;
            typeOutTextDN.enabled = false;
            typeOutTextGF.enabled = false;

        }

        if (inputField == answer2)
        { 
            if (playerController.x == 3 && playerController.y == 2 && textblock.floor == 2)
            {
                typeOutTextDN.enabled = true;
                typeOutTextDN.devroom = true;
                typeOutTextFR.enabled = false;
                typeOutTextGF.enabled = false;
            }
        }

        if (inputField == answer3)
        {
            typeOutTextGF.gameFin = true;
            typeOutTextGF.enabled = true;
            typeOutTextFR.enabled = false;
            typeOutTextDN.enabled = false;


        }
    }

    
}

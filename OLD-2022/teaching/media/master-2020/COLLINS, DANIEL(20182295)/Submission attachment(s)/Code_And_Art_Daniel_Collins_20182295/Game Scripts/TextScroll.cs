﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class TextScroll : MonoBehaviour
{
    public TMP_Text m_TextComponent;
    private bool hasTextChanged;
    public bool start = false;
    public bool devroom = false;
    public bool gameFin = false;
    public TMP_Text devtext;
    public TMP_Text gamefin;

    float waittime;
    void Awake()
    {
        m_TextComponent = gameObject.GetComponent<TMP_Text>();
        devtext = gameObject.GetComponent<TMP_Text>();
        gamefin = gameObject.GetComponent<TMP_Text>();
    }


    void Update()
    {
        if (start == true)
        {
            StartCoroutine(RevealCharacters(m_TextComponent));
            
        }
        if (start == false)
        {
            StopCoroutine(RevealCharacters(m_TextComponent));
        }

        if (devroom == true)
        {
            StartCoroutine(RevealCharacters2(devtext));
           
        }
        if (gameFin == true)
        {
            StartCoroutine(RevealCharacters3(gamefin));

        }
        //StartCoroutine(RevealWords(m_TextComponent));


    }


    void OnEnable()
    {
        // Subscribe to event fired when text object has been regenerated.
        TMPro_EventManager.TEXT_CHANGED_EVENT.Add(ON_TEXT_CHANGED);
    }

    void OnDisable()
    {
        TMPro_EventManager.TEXT_CHANGED_EVENT.Remove(ON_TEXT_CHANGED);
    }


    // Event received when the text object has changed.
    void ON_TEXT_CHANGED(Object obj)
    {
        hasTextChanged = true;
    }


    /// <summary>
    /// Method revealing the text one character at a time.
    /// </summary>
    /// <returns></returns>
   public IEnumerator RevealCharacters(TMP_Text textComponent)
    {
        textComponent.enabled = true;
        textComponent.ForceMeshUpdate();

        TMP_TextInfo textInfo = textComponent.textInfo;

        int totalVisibleCharacters = textInfo.characterCount; // Get # of Visible Character in text object
        int visibleCount = 0;

        while (true)
        {
            if (hasTextChanged)
            {
                totalVisibleCharacters = textInfo.characterCount; // Update visible character count.
                hasTextChanged = false;
            }
            
                if (visibleCount > totalVisibleCharacters)
                {
                if (Input.GetKeyUp(KeyCode.KeypadEnter))
                {
                    textComponent.enabled = false;
                }
                    yield return new WaitForSeconds(20f);
                    textComponent.enabled = false;
                // visibleCount = 0;
            }
            

            textComponent.maxVisibleCharacters = visibleCount; // How many characters should TextMeshPro display?

            visibleCount += 1;

            yield return null;
        }
    }
    public IEnumerator RevealCharacters2(TMP_Text textComponent)
    {
        textComponent.enabled = true;
        textComponent.ForceMeshUpdate();

        TMP_TextInfo textInfo = textComponent.textInfo;

        int totalVisibleCharacters = textInfo.characterCount; // Get # of Visible Character in text object
        int visibleCount = 0;

        while (true)
        {
            if (hasTextChanged)
            {
                totalVisibleCharacters = textInfo.characterCount; // Update visible character count.
                hasTextChanged = false;
            }

            if (visibleCount > totalVisibleCharacters)
            {
                if (Input.GetKeyUp(KeyCode.KeypadEnter))
                {
                    textComponent.enabled = false;
                }
                yield return new WaitForSeconds(20f);
                textComponent.enabled = false;
                // visibleCount = 0;
            }


            textComponent.maxVisibleCharacters = visibleCount; // How many characters should TextMeshPro display?

            visibleCount += 1;

            yield return null;
        }
    }

    public IEnumerator RevealCharacters3(TMP_Text textComponent)
    {
        textComponent.enabled = true;
        textComponent.ForceMeshUpdate();

        TMP_TextInfo textInfo = textComponent.textInfo;

        int totalVisibleCharacters = textInfo.characterCount; // Get # of Visible Character in text object
        int visibleCount = 0;

        while (true)
        {
            if (hasTextChanged)
            {
                totalVisibleCharacters = textInfo.characterCount; // Update visible character count.
                hasTextChanged = false;
            }

            if (visibleCount > totalVisibleCharacters)
            {
                if (Input.GetKeyUp(KeyCode.KeypadEnter))
                {
                    textComponent.enabled = false;
                }
                yield return new WaitForSeconds(20f);
                textComponent.enabled = false;
                // visibleCount = 0;
            }


            textComponent.maxVisibleCharacters = visibleCount; // How many characters should TextMeshPro display?

            visibleCount += 1;

            yield return null;
        }
    }



}


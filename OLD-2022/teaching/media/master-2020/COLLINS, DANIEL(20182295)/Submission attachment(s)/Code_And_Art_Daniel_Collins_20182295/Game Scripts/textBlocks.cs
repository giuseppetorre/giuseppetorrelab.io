﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class textBlocks : MonoBehaviour
{
    public TextControls textController;
    public InputController inputCon;

    string insertText;

    public GameObject map1;
    public GameObject map2;

    bool gs;

    public int floor;

    #region rooms 
    bool keyroomsSuicide = false;
    bool keyroomsMurder = false;
    bool keyroomsAnomoly = false;

    #endregion

    void Start()
    {
        map1.GetComponent<SpriteRenderer>().enabled = true;
        map2.GetComponent<SpriteRenderer>().enabled = false;
        // Hey Jeff, didn't we agree to have this done by last friday ? 
        // Ya thats what we had discussed in the meeting but I wasn't able to get it done because nick dropped more work on my desk for the script. 
        // Get it done by Wednesday or you're out of here!
        // If im out of here I'm taking all my work with me! there wont be a game left for you to make.
        // You're easily replaceable Jeff. Don't get smart with me. Get it done now!

        floor = 1;


    }

    void Update()
    {

        #region groundfloor
        if (inputCon.x == 2 && inputCon.y == 1 && floor == 1)
        {   //entrance    
            inputCon.mapPos.transform.position = new Vector2(106f, 60f);
            map1.GetComponent<SpriteRenderer>().enabled = true;
            map2.GetComponent<SpriteRenderer>().enabled = false;
            if (Input.GetKey(KeyCode.I))
            {
                mainEntranceInvest();
            }
            else
            {
                entrance();
            }
        }
        if (inputCon.x == 2 && inputCon.y == 2 && floor == 1)
        {   //main hall
            inputCon.mapPos.transform.position = new Vector2(107f, 78f);
            map1.GetComponent<SpriteRenderer>().enabled = true;
            map2.GetComponent<SpriteRenderer>().enabled = false;
            if (textController.deathType == 2)
            {
                mainHallMKR();
                if (Input.GetKey(KeyCode.I))
                {
                    HallKeyInvest();
                }
                if (textController.murderRoom == 3)
                {
                    mainHallMDR();
                    if (Input.GetKey(KeyCode.I))
                    {
                        HallDeathInvest();
                    }
                }
            }
            else
            {
                mainHall();
                if (Input.GetKey(KeyCode.I))
                {
                    HallInvest();
                }
            }

        }
        if (inputCon.x == 2 && inputCon.y == 3)
        {    //stairs       
            inputCon.mapPos.transform.position = new Vector2(107f, 104f);
            if (Input.GetKey(KeyCode.I))
            {
                stairsInvest();
            }
            else
            {
                stairway();
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                floor = 2;
                map1.GetComponent<SpriteRenderer>().enabled = false;
                map2.GetComponent<SpriteRenderer>().enabled = true;
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                floor = 1;
                map1.GetComponent<SpriteRenderer>().enabled = true;
                map2.GetComponent<SpriteRenderer>().enabled = false;
            }
        }
        if (inputCon.x == 3 && inputCon.y == 2 && floor == 1)
        {   //dining room         
            inputCon.mapPos.transform.position = new Vector2(133f, 82f);
            map1.GetComponent<SpriteRenderer>().enabled = true;
            map2.GetComponent<SpriteRenderer>().enabled = false;
            if (textController.deathType == 2)
            {
                diningRoomMKR();
                if (Input.GetKey(KeyCode.I))
                {
                    DiningRoomKeyInvest();
                }
                if (textController.murderRoom == 4)
                {
                    diningRoomMDR();
                    if (Input.GetKey(KeyCode.I))
                    {
                        DiningRoomDeathInvest();
                    }
                }
            }
            else
            {
                diningRoom();
            }
        }
        if (inputCon.x == 3 && inputCon.y == 3 && floor == 1)
        {   //kitchen
            inputCon.mapPos.transform.position = new Vector2(133f, 102f);
            map1.GetComponent<SpriteRenderer>().enabled = true;
            map2.GetComponent<SpriteRenderer>().enabled = false;
            if (textController.deathType == 1)
            {
                kitchenSKR();
                if (Input.GetKey(KeyCode.I))
                {
                    KitchenSKeyInvest();
                }
                if (textController.suicideRoom == 2)
                {
                    kitchenSDR();
                    if (Input.GetKey(KeyCode.I))
                    {
                        KitchenSInvest();
                    }
                }
            }
            else if (textController.deathType == 2)
            {
                kitchenMKR();
                if (Input.GetKey(KeyCode.I))
                {
                    KitchenMKeyInvest();
                }
                if (textController.murderRoom == 2)
                {
                    kitchenMDR();
                    if (Input.GetKey(KeyCode.I))
                    {
                        KitchenMInvest();
                    }
                }
            }
            else
            {
                kitchen();              
            }
        }
        if (inputCon.x == 1 && inputCon.y == 2 && floor == 1)
        {   //livingroom
            inputCon.mapPos.transform.position = new Vector2(81f, 82f);
            map1.GetComponent<SpriteRenderer>().enabled = true;
            map2.GetComponent<SpriteRenderer>().enabled = false;
            if (Input.GetKey(KeyCode.I))
            {
                livingroomInvest();
            }
            else
            {
                livingRoom();
            }
        }
        if (inputCon.x == 1 && inputCon.y == 3 && floor == 1)
        {   //study      
            inputCon.mapPos.transform.position = new Vector2(81f, 102f);
            map1.GetComponent<SpriteRenderer>().enabled = true;
            map2.GetComponent<SpriteRenderer>().enabled = false;
            if (textController.deathType == 1)
            {
                studySKR();
                if (Input.GetKey(KeyCode.I))
                {
                    StudyKeyInvest();
                }
                if (textController.suicideRoom == 3)
                {                    
                    studySDR();
                    if (Input.GetKey(KeyCode.I))
                    {
                        StudyDeathInvest();
                    }
                }

            }
            else
            {
                study();
                if (Input.GetKey(KeyCode.I))
                {
                    StudyInvest();
                }
            }
        }

        #endregion

        #region upperfloor      
        if (inputCon.x == 2 && inputCon.y == 2 && floor == 2)
        {         
            inputCon.mapPos.transform.position = new Vector2(106f, 63f);
            map1.GetComponent<SpriteRenderer>().enabled = false;
            map2.GetComponent<SpriteRenderer>().enabled = true;
            if (textController.deathType == 1)
            {
                masterBedroomSKR();
                if (Input.GetKey(KeyCode.I))
                {
                    MasterBedroomSKeyInvest();
                }
                if (textController.suicideRoom == 1)
                {
                    masterBedroomSDR();
                    if (Input.GetKey(KeyCode.I))
                    {
                        MasterBedroomSInvest();
                    }
                }
            }
            else if (textController.deathType == 2)
            {
                masterBedroomMKR();
                if (Input.GetKey(KeyCode.I))
                {
                    MasterBedroomMKeyInvest();
                }
                if (textController.murderRoom == 1)
                {
                    masterBedroomMDR();
                    if (Input.GetKey(KeyCode.I))
                    {
                        MasterBedroomMInvest();
                    }
                }
            }
            else
            {
                masterBedroom();
            }

        }

        if (inputCon.x == 3 && inputCon.y == 2 && floor == 2)
        {
            inputCon.mapPos.transform.position = new Vector2(136f, 80f);
            map1.GetComponent<SpriteRenderer>().enabled = false;
            map2.GetComponent<SpriteRenderer>().enabled = true;
            if (Input.GetKey(KeyCode.I))
            {
                storageClosetInvest();
            }
            else
            {
                storageCloset();
            }
        }

        if (inputCon.x == 3 && inputCon.y == 3 && floor == 2)
        {
            inputCon.mapPos.transform.position = new Vector2(136f, 99f);
            map1.GetComponent<SpriteRenderer>().enabled = false;
            map2.GetComponent<SpriteRenderer>().enabled = true;
            if (textController.deathType == 1)
            {
                bathroomSKR();
                if (Input.GetKey(KeyCode.I))
                {
                    BathroomKeyInvest();
                }
                if (textController.suicideRoom == 4)
                {
                    bathroomSDR();
                    if (Input.GetKey(KeyCode.I))
                    {
                        BathroomInvest();
                    }
                }
            }
            else
            {
                bathroom();
                if (Input.GetKey(KeyCode.I))
                {
                    BathroomInvest();
                }
            }

        }

        if (inputCon.x == 1 && inputCon.y == 2 && floor == 2)
        {           
            inputCon.mapPos.transform.position = new Vector2(78f, 80f);
            map1.GetComponent<SpriteRenderer>().enabled = false;
            map2.GetComponent<SpriteRenderer>().enabled = true;
            if (Input.GetKey(KeyCode.I))
            {
                guestBedroomInvest();
            }
            else
            {
                guestBedroom();
            }
        }

        if (inputCon.x == 1 && inputCon.y == 3 && floor == 2)
        {
            inputCon.mapPos.transform.position = new Vector2(78f, 99f);
            map1.GetComponent<SpriteRenderer>().enabled = false;
            map2.GetComponent<SpriteRenderer>().enabled = true;
            if (Input.GetKey(KeyCode.I))
            {
               bedroomInvest();
            }
            else
            {
                bedroom();
            }
        }
        #endregion        

    }


    #region downstairs text
    public void entrance()
    {
        insertText = "<align=center><#8c8c8c><size=46>Main Entrance" +
            "\n</align><#ffffff><size=36>The entrance to the manor is dimly lit. the main hall is straight ahead.";
        textController.textObject.text = insertText;
        inputCon.canGoN = true;
        inputCon.canGoS = false;
        inputCon.canGoE = false;
        inputCon.canGoW = false;
    }

    public void mainHall()
    {
        insertText = "<align=center><#8c8c8c><size=46>Manor Main Hall" +
            "\n</align><#ffffff><size=36>The main hall of the manor leads straight to the stairs. To the left is the living room and to the right is the dining room.";
        textController.textObject.text = insertText;
        inputCon.canGoN = true;
        inputCon.canGoS = true;
        inputCon.canGoE = true;
        inputCon.canGoW = true;
    }

    public void stairway()
    {
        insertText = "<align=center><#8c8c8c><size=46>Stairway" + "\n</align><#ffffff><size=36>The stairs have one entrance, a landing and two sets of stairs leading " +
            "to the left and right wing of the manor" + "\n\n<align=center><#8c8c8c>press A to assend and D to descend";
        textController.textObject.text = insertText;
        inputCon.canGoN = false;
        inputCon.canGoS = true;
        inputCon.canGoE = true;
        inputCon.canGoW = true;
    }

    public void kitchen()
    {
        insertText = "<align=center><#8c8c8c><size=46>Kitchen" + "\n</align><#ffffff><size=36>The passage leads straigh ahead, " +
            "the roadway is wide enough for 2 vehicles with a dyke on either side.";
        textController.textObject.text = insertText;
        inputCon.canGoN = false;
        inputCon.canGoS = true;
        inputCon.canGoE = false;
        inputCon.canGoW = false;
    }

    public void diningRoom()
    {
        insertText = "<align=center><#8c8c8c><size=46>Dining Room" + "\n</align><#ffffff><size=36>The table is set out to seat 10 people. The finest silver cutlery is placed neatly at each chair."
            + "A crystal candle holder sits in the center of the table, the candles melted to the end.";
        textController.textObject.text = insertText;
        inputCon.canGoN = true;
        inputCon.canGoS = false;
        inputCon.canGoE = false;
        inputCon.canGoW = true;
    }

    public void study()
    {
        insertText = "<align=center><#8c8c8c><size=46>Study" + "\n</align><#ffffff><size=36>A room not often tidied by the cleaner. The late lords study is home to all of his literature and writing material.";
        textController.textObject.text = insertText;
        inputCon.canGoN = false;
        inputCon.canGoS = true;
        inputCon.canGoE = false;
        inputCon.canGoW = false;
    }
    public void livingRoom()
    {
        insertText = "<align=center><#8c8c8c><size=46>Living Room" + "\n</align><#ffffff><size=36>An antiquely furnished room comfortably heated with a fire place. There are several skotch glasses and a bottle half empty laying on a side table.";
        textController.textObject.text = insertText;
        inputCon.canGoN = true;
        inputCon.canGoS = false;
        inputCon.canGoE = true;
        inputCon.canGoW = false;
    }
    #endregion

    #region upstairs text
    public void masterBedroom()
    {
        insertText = "<align=center><#8c8c8c><size=46>Master Bedroom" +
            "\n</align><#ffffff><size=36>The crossroads go one of four directions, north to the manor, south to the village, east to the temple and west to leave town,";
        textController.textObject.text = insertText;
        inputCon.canGoN = true;
        inputCon.canGoS = false;
        inputCon.canGoE = true;
        inputCon.canGoW = true;

    }

    public void bathroom()
    {
        insertText = "<align=center><#8c8c8c><size=46>Bathroom" +
            "\n</align><#ffffff><size=36>A simple bathroom with a shower replacing the once existing bathtub.";
        textController.textObject.text = insertText;
        inputCon.canGoN = false;
        inputCon.canGoS = true;
        inputCon.canGoE = false;
        inputCon.canGoW = true;
    }

    public void guestBedroom()
    {
        insertText = "<align=center><#8c8c8c><size=46>Guest Bedroom" +
            "\n</align><#ffffff><size=36>A cold freshly used  room for the guests of the manor fitted with fine silk sheets and curtins.";
        textController.textObject.text = insertText;
        inputCon.canGoN = true;
        inputCon.canGoS = false;
        inputCon.canGoE = true;
        inputCon.canGoW = false;
    }

    public void storageCloset()
    {
        insertText = "<align=center><#8c8c8c><size=46>Storage Closet" +
            "\n</align><#ffffff><size=36>A Coat Closet filled with posh fur coats and jackets.";
        textController.textObject.text = insertText;
        inputCon.canGoN = true;
        inputCon.canGoS = false;
        inputCon.canGoE = false;
        inputCon.canGoW = true;
    }

    public void bedroom()
    {
        insertText = "<align=center><#8c8c8c><size=46>bedroom" +
            "\n</align><#ffffff><size=36>The bedroom is neatly kept. pictures, placks, awards and trinkets line the walls and shelves.";
        textController.textObject.text = insertText;
        inputCon.canGoN = false;
        inputCon.canGoS = true;
        inputCon.canGoE = true;
        inputCon.canGoW = false;
    }
    #endregion


    #region suicide rooms text
    //master bedroom
    public void masterBedroomSDR()
    {
        insertText = "<align=center><#8c8c8c><size=46>Master Bedroom" +
            "\n</align><#ffffff><size=36>The death occurred on the bed. Many items are scattered around the room with <#fc8803>a bottle of skotch shattered against the wall. <#ffffff>Blood leads from the bottle to the bed.";
        textController.textObject.text = insertText;
        inputCon.canGoN = true;
        inputCon.canGoS = false;
        inputCon.canGoE = true;
        inputCon.canGoW = true;
    }
    public void masterBedroomSKR()
    {
        insertText = "<align=center><#8c8c8c><size=46>Master Bedroom" +
            "\n</align><#ffffff><size=36>Many items are scattered around the room with <#fc8803>a bottle of skotch <#ffffff>shattered against the wall. Drawrs and presses are opened with <#fc8803>notes <#ffffff>scattered throughout";
        textController.textObject.text = insertText;
        inputCon.canGoN = true;
        inputCon.canGoS = false;
        inputCon.canGoE = true;
        inputCon.canGoW = true;
    }
    // bathroom
    public void bathroomSDR()
    {
        insertText = "<align=center><#8c8c8c><size=46>Bathroom" +
            "\n</align><#ffffff><size=36>Blood covers the shower and curtins. <#fc8803>Razor blades <#ffffff>and medicine cover the floors. The sink is filled with watered down blood. The body was found in the shower.";
        textController.textObject.text = insertText;
        inputCon.canGoN = false;
        inputCon.canGoS = true;
        inputCon.canGoE = false;
        inputCon.canGoW = true;
    }
    public void bathroomSKR()
    {
        insertText = "<align=center><#8c8c8c><size=46>Bathroom" +
            "\n</align><#ffffff><size=36>Blood covers the shower and curtins. <#fc8803>Razor blades <#ffffff>and medicine cover the floors. The sink is filled with watered down blood.";
        textController.textObject.text = insertText;
        inputCon.canGoN = false;
        inputCon.canGoS = true;
        inputCon.canGoE = false;
        inputCon.canGoW = true;
    }
    //study
    public void studySDR()
    {
        insertText = "<align=center><#8c8c8c><size=46>Study" +
            "\n</align><#ffffff><size=36>Blood is splattered all over the <#fc8803>back wall and ceiling. <#ffffff>A Smith & Wesson revolver is <#fc8803>on the ground <#ffffff>not far from the desk and where the body was found.";
        textController.textObject.text = insertText;
        inputCon.canGoN = false;
        inputCon.canGoS = true;
        inputCon.canGoE = false;
        inputCon.canGoW = false;
    }
    public void studySKR()
    {
        insertText = "<align=center><#8c8c8c><size=46>Study" +
            "\n</align><#ffffff><size=36>A <#fc8803>sealed note <#ffffff>lay on the table. The contents contain the late lords will. Besides some books from the shelves scattered on the floor in piles nothing seems out of place except the lords medication on the table.";
        textController.textObject.text = insertText;
        inputCon.canGoN = false;
        inputCon.canGoS = true;
        inputCon.canGoE = false;
        inputCon.canGoW = false;
    }
    //kitchen
    public void kitchenSDR()
    {
        insertText = "<align=center><#8c8c8c><size=46>Kitchen" +
            "\n</align><#ffffff><size=36>Nothing at all seems out of the ordinary. a glass of water half full sits on the table beside some <#fc8803>paper and writing utensils";
        textController.textObject.text = insertText;
        inputCon.canGoN = false;
        inputCon.canGoS = true;
        inputCon.canGoE = false;
        inputCon.canGoW = false;
    }
    public void kitchenSKR()
    {
        insertText = "<align=center><#8c8c8c><size=46>Kitchen" +
            "\n</align><#ffffff><size=36>Nothing at all seems out of the ordinary. A few knifes are missing from the knife block, found around the room except for <#fc8803>one that is missing.";
        textController.textObject.text = insertText;
        inputCon.canGoN = false;
        inputCon.canGoS = true;
        inputCon.canGoE = false;
        inputCon.canGoW = false;
    }
    #endregion

    #region murder rooms text
    //master bedroom
    public void masterBedroomMDR()
    {
        insertText = "<align=center><#8c8c8c><size=46>Master Bedroom" +
            "\n</align><#ffffff><size=36>The death occurred on the bed. Many items are scattered around the room with <#fc8803>a bottle of skotch shattered against the wall. <#ffffff>Blood leads from the bottle to the bed. <#fc8803>The glass trail leads to the bed";
        textController.textObject.text = insertText;
        inputCon.canGoN = true;
        inputCon.canGoS = false;
        inputCon.canGoE = true;
        inputCon.canGoW = true;
    }
    public void masterBedroomMKR()
    {
        insertText = "<align=center><#8c8c8c><size=46>Master Bedroom" +
            "\n</align><#ffffff><size=36>Many items are scattered around the room with <#fc8803>a bottle of skotch <#ffffff>shattered against the wall. Drawrs and presses are opened with <#fc8803>notes <#ffffff>scattered throughout";
        textController.textObject.text = insertText;
        inputCon.canGoN = true;
        inputCon.canGoS = false;
        inputCon.canGoE = true;
        inputCon.canGoW = true;
    }
    // Hallway
    public void mainHallMDR()
    {
        insertText = "<align=center><#8c8c8c><size=46>Manor Main Hall" +
            "\n</align><#ffffff><size=36>A large <#fc8803>dried pool of blood <#ffffff>stains the hallway rug. The rug appears to have <#fc8803>gash marks <#ffffff>burried deep into it.";
        textController.textObject.text = insertText;
        inputCon.canGoN = true;
        inputCon.canGoS = true;
        inputCon.canGoE = true;
        inputCon.canGoW = true;
    }
    public void mainHallMKR()
    {
        insertText = "<align=center><#8c8c8c><size=46>Manor Main Hall" +
            "\n</align><#ffffff><size=36>Remenence of <#fc8803>footprints covered in blood <#ffffff>stain the hallway rug. The footprints apear to match the <#fc8803>suspects.";
        textController.textObject.text = insertText;
        inputCon.canGoN = true;
        inputCon.canGoS = true;
        inputCon.canGoE = true;
        inputCon.canGoW = true;
    }
    //dining room
    public void diningRoomMDR()
    {
        insertText = "<align=center><#8c8c8c><size=46>Dining Room" +
            "\n</align><#ffffff><size=36>The body was found seated at the table with <#fc8803>food half eaten still out. <#ffffff>There was no sign of a struggle or distress.";
        textController.textObject.text = insertText;
        inputCon.canGoN = true;
        inputCon.canGoS = false;
        inputCon.canGoE = false;
        inputCon.canGoW = true;
    }
    public void diningRoomMKR()
    {
        insertText = "<align=center><#8c8c8c><size=46>Dining Room" +
            "\n</align><#ffffff><size=36>Besides the <#fc8803>half eaten food, <#ffffff> there is noting strange here except some <#fc8803>sicky stains <#ffffff>on the back of the chair where the lord was found.";
        textController.textObject.text = insertText;
        inputCon.canGoN = true;
        inputCon.canGoS = false;
        inputCon.canGoE = false;
        inputCon.canGoW = true;
    }
    //kitchen
    public void kitchenMDR()
    {
        insertText = "<align=center><#8c8c8c><size=46>Kitchen" +
            "\n</align><#ffffff><size=36>Nothing at all seems out of the ordinary except a glass is <#fc8803>broken <#ffffff>across the table beside some <#fc8803>paper and writing utensils";
        textController.textObject.text = insertText;
        inputCon.canGoN = false;
        inputCon.canGoS = true;
        inputCon.canGoE = false;
        inputCon.canGoW = false;
    }
    public void kitchenMKR()
    {
        insertText = "<align=center><#8c8c8c><size=46>Kitchen" +
            "\n</align><#ffffff><size=36>Nothing at all seems out of the ordinary. A few knifes are missing from the knife block, found around the room except for <#fc8803>one that is missing.";
        textController.textObject.text = insertText;
        inputCon.canGoN = false;
        inputCon.canGoS = true;
        inputCon.canGoE = false;
        inputCon.canGoW = false;
    }
    #endregion


    #region investigations
    void mainEntranceInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Main Entrance Investigation" +
            "\n</align><#ffffff><size=36>Overdue notices lay open on the entryway table scattered and piled up by a small orniment.";
        textController.textObject.text = insertText;
    }
    void stairsInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Stair Way Investigation" +
            "\n</align><#ffffff><size=36>There are a few dried in coffee stains on the carpet. On the back wall is a freamed oil painting titled 'Dogs Playing Poker'.";
        textController.textObject.text = insertText;
    }
    void bedroomInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Bedroom Investigation" +
            "\n</align><#ffffff><size=36>Insignia of war lay neatly placed on shelves. One, a badge enscribed TF141 lay in a glass box as a merit of service.";
        textController.textObject.text = insertText;
    }
    void guestBedroomInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Guest Bedroom Investigation" +
            "\n</align><#ffffff><size=36>A chess set lay set out on the side table, the pieces made of alabaster and soapstone, a name is engraved into the side 'Randal Stephens'.";
        textController.textObject.text = insertText;
    }
    void storageClosetInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Storage Closet Investigation" +
            "\n</align><#ffffff><size=36>A brown suitcase lay in the corner marked with the letter 'L'. A blace top hat lay on the case fit with the red trim.";
        textController.textObject.text = insertText;
    }
    void livingroomInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Living Room Investigation" +
           "\n</align><#ffffff><size=36>An Antiquely furnished living room. A lever action rifle is on a stand with the name 'Morgan.A' on a plaque below. A framed map of the old west sits above the mantel.";
        textController.textObject.text = insertText;
    }
    #endregion

    #region other investigations
    //hallway
    void HallInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Manor hall Investigation" +
            "\n</align><#ffffff><size=36>The hall is furnished with aristocratic paintings hung on the wall. A large crystal chandelier hangs grom the second floor lighting the upstairs and hallway.";
        textController.textObject.text = insertText;      
    }
    void HallKeyInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Manor Hall Investigation" +
            "\n</align><#ffffff><size=36>Further investigation of the footprints find that they are an exact match to the suspect. However, they lead towards the entrance rather than the house.";
        textController.textObject.text = insertText;        
    }
    void HallDeathInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Main Entrance Investigation" +
            "\n</align><#ffffff><size=36>The white chalk line marks where the body was found. A knife from the kitchen is found under a side table.";
        textController.textObject.text = insertText;       
    }
    //dining room
    void DiningRoomInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Dining Room Investigation" +
            "\n</align><#ffffff><size=36>The table looks as though it was used recently. A gravy stain marks the table cloth where perhaps a cluts spit some.";
        textController.textObject.text = insertText;
    }
    void DiningRoomKeyInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Dining Room Investigation" +
            "\n</align><#ffffff><size=36>The Sticky Stains appear to be a pision of some kind. The half eaten food has on it a similar sticky substance.";
        textController.textObject.text = insertText;
    }
    void DiningRoomDeathInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Dining Room Investigation" +
            "\n</align><#ffffff><size=36>The Sticky Stains appear to be a pision of some kind. The half eaten food has on it a similar sticky substance. the lord was found seated at this seat of the table.";
        textController.textObject.text = insertText;
    }
    //study
    void StudyInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Study Investigation" +
            "\n</align><#ffffff><size=36>The late lord was meticulous in what tools he used to write. A fine tipped quill made from a white owls feather is placed neatly on a napkin by ink container branded 'Flourish and Blotts'.";
        textController.textObject.text = insertText;
    }
    void StudyKeyInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Study Investigation" +
            "\n</align><#ffffff><size=36>Further investigation of the lords mediaction reviel he was taking heart medication however, he may have forgot to bring them with him before he died.";
        textController.textObject.text = insertText;
    }
    void StudyDeathInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Study Investigation" +
            "\n</align><#ffffff><size=36>There is a box of 38 Special rounds scattered around the table. The amunition was perhaps scattered in a hurry or out of nervousness. Only the lords prints were found in the room.";
        textController.textObject.text = insertText;
    }
    //bathroom
    void BathroomInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Bathroom Investigation" +
            "\n</align><#ffffff><size=36>The medication cabinet is stocked with pills of all sorts. The bathroom has been modernised despite the rest of the house keeping with a antique feel.";
        textController.textObject.text = insertText;
    }
    void BathroomKeyInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Bathroom Investigation" +
            "\n</align><#ffffff><size=36>Some glass was found on the floor, perhaps the lord had cut himself and was panicing to find a bandage as the first aid box is open in the cabinet.";
        textController.textObject.text = insertText;
    }
    void BathroomDeathInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Bathroom Investigation" +
            "\n</align><#ffffff><size=36>The razor blades are covered in blood. There are one or two in the shower where the body was found.";
        textController.textObject.text = insertText;
    }

    #endregion

    #region common rooms investigation
    //master bedroom
    void MasterBedroomSInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Master Bedroom Investigation" +
            "\n</align><#ffffff><size=36>Inside one of the dressers is a note adressed to the late lords son. It appears to be an apology note written in the lords handwriting.";
        textController.textObject.text = insertText;
    }
    void MasterBedroomSKeyInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Master Bedroom Investigation" +
            "\n</align><#ffffff><size=36>The trail of blood stops after the bed. Pillow covers are missing that were found with the body.";
        textController.textObject.text = insertText;
    }
    void MasterBedroomMInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Master Bedroom Investigation" +
            "\n</align><#ffffff><size=36>A shard of glass was found under the pillow with a fingerprint matching the suspects. It seems that there was a struggle of some kind.";
        textController.textObject.text = insertText;
    }
    void MasterBedroomMKeyInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Master Bedroom Investigation" +
            "\n</align><#ffffff><size=36>Some of the glass from the bottle is missing. Some jewellery is also missing from a box on the dresser.";
        textController.textObject.text = insertText;
    }

    //kitchen
    void KitchenSInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Kitchen Investigation" +
            "\n</align><#ffffff><size=36>The resedue of some pill remains on the paper. forensics show that this was not medication perscribed to the lord but the lady of the manor.";
        textController.textObject.text = insertText;
    }
    void KitchenSKeyInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Kitchen Investigation" +
            "\n</align><#ffffff><size=36>The knife missing from the kitchen was found at the back door used to open a sack of potatoes. There was a glass of water half empty on the counter with the lords dna on it.";
        textController.textObject.text = insertText;
    }
    void KitchenMInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Kitchen Investigation" +
            "\n</align><#ffffff><size=36>There was a knife missing from the block and a knife found stabbed in the lords hand when the body was discovered.";
        textController.textObject.text = insertText;
    }
    void KitchenMKeyInvest()
    {
        insertText = "<align=center><#8c8c8c><size=46>Kitchen Investigation" +
            "\n</align><#ffffff><size=36>The missing Knife was recoverd in the hallway. there was blood on the knife that matched the lords dna.";
        textController.textObject.text = insertText;
    }
    #endregion
}

/*
 "<align=center><#8c8c8c><size=46>title" + "\n</align><#ffffff><size=36>body " +
            "body";
            
 "<align=center><#8c8c8c><size=46>Manor Door" + "\n</align><#ffffff><size=36>The door swings loosly off its hinges. " +
            "Inside is the main halway, the living room is to the left and the kitchen to the right. a note is on the ground you should investigate";
     
     
 "<align=center><#8c8c8c><size=46>Main corridor" + "\n</align><#ffffff><size=36>To the left leads to the first bedroom, to the right a bathroom" +
            "straight ahead is the master study"; 
     
    // research numerology / remaro / anthology / algorithmic  

     <#fc8803> orange
     <#fc8803> blue
     <#c90606> red
     <#c90606> green
     <#fc8803> white
     <#949494> grey
*/

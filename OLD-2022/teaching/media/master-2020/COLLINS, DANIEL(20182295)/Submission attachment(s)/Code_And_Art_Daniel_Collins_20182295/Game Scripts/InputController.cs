﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{

    #region values
    public int x = 0;
    public int y = 0;
    bool gameStarted = false;
    #endregion

    public GameObject mapPos;
    public SpriteRenderer startDirections;
    public SpriteRenderer inGameDirections;

    public Animator heartrate;

    public bool canGoN;
    public bool canGoS;
    public bool canGoE;
    public bool canGoW;

    void Start()
    {
        startDirections.GetComponent<SpriteRenderer>();
        inGameDirections.GetComponent<SpriteRenderer>();
        heartrate.GetComponent<Animator>();     
    }

 
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.UpArrow) )
        {
            if (canGoN)
            {
                y++;
            }
        }
        if (Input.GetKeyUp(KeyCode.DownArrow) )
        {
            if (canGoS)
            {
                y--;
            }            
        }
        if (Input.GetKeyUp(KeyCode.RightArrow) )
        {
            if (canGoE)
            {
                x++;
            }
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow) )
        {
            if (canGoW)
            {
                x--;
            }
        }
        if (Input.anyKey && x == 0 && y == 0)
        {            
            gameStarted = true;
            StartCoroutine(gamestarted());
        }

        if (gameStarted == false)
        {
            inGameDirections.enabled = false;
            startDirections.enabled = true;
            canGoN = true;
            canGoS = true;
            canGoE = true;
            canGoW = true;
        }
        else if (gameStarted == true)
        {
            inGameDirections.enabled = true;
            startDirections.enabled = false;       
        }
    }

    IEnumerator gamestarted()
    {
        yield return new WaitForSeconds(.15f);
        x = 2;
        y = 1;
        yield return null; 
    }
}

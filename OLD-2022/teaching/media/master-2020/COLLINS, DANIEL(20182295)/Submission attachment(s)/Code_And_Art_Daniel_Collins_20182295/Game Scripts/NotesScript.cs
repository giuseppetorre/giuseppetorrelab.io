﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NotesScript : MonoBehaviour
{
    public TMP_InputField inputField;
    public TextControls causeOfDeath;
    public string correctAnswer;

    public GameObject submit;
    public SpriteRenderer blackscreen;
    public GameObject notestext;
    public GameObject investigatetext;
    public TextMeshProUGUI maintxt;
    public CmdEasterEgg egg;
    public GameObject inputControlls;
    bool giveanswer = false;
    public SpriteRenderer noteSheet;
    public TextMeshProUGUI rightorwrongtext;
    string answer;


    private void Start()
    {
        rightorwrongtext.GetComponent<TextMeshProUGUI>();
        StartCoroutine(setwin());
    }

    private void Update()
    {
       

        if (Input.GetKeyDown(KeyCode.S) && egg.cmdOn == false)
        {
            causeOfDeath.canRestart = false;
            submit.SetActive(true);
            inputControlls.SetActive(false);
            blackscreen.enabled = true;
            maintxt.enabled = false;
            notestext.SetActive(false);
            investigatetext.SetActive(false);
            giveanswer = true;
            noteSheet.enabled = true;
            rightorwrongtext.text = "Was the death a Murder or Suicide \n\n Input answer _______________";

        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            causeOfDeath.canRestart = true;
            submit.SetActive(false);
            inputControlls.SetActive(true);
            blackscreen.enabled = false;
            maintxt.enabled = true;
            notestext.SetActive(true);
            investigatetext.SetActive(true);
            giveanswer = false;
            noteSheet.enabled = false;
        }
        
            
        
    }

    public void testString(string answer)
    {
        Debug.Log("testing answer");
        if (answer == correctAnswer)
        {
            Debug.Log("correct");
            rightorwrongtext.text = "<align=center><size=45><#38e00d>Correct!";
            StartCoroutine(rightAnswer());
            inputField.text = "";
        }
        else if(answer != correctAnswer)
        {
            rightorwrongtext.text = "<align=center><size=45><#e0260d>Wrong!";
            StartCoroutine(wrongAnswer());
            inputField.text = "";
            Debug.Log("wrong");
        }
       
    }
    public void giveAnswer()
    {
        answer = inputField.GetComponent<TMP_InputField>().text;
        testString(answer);
    }

    IEnumerator setwin()
    {
        yield return new WaitForSeconds(1);
        if (causeOfDeath.deathType == 1)
        {            
            correctAnswer = "suicide";
        }
        else if (causeOfDeath.deathType == 2)
        {
            correctAnswer = "murder";
        }
        Debug.Log(correctAnswer);
    }

    IEnumerator wrongAnswer()
    {
        yield return new WaitForSeconds(3);
        if (causeOfDeath.deathType == 1)
        {
            
            rightorwrongtext.text = "<align=center><size=34><#e0260d>An inocent man went to prison because of the choice you made! \nPress R to restart";
        }
        else if (causeOfDeath.deathType == 2)
        {
            rightorwrongtext.text = "<align=center><size=34><#e0260d>A murderer got away with a horible crime because of the choice you made! \nPress R to restart";
        }
    }

    IEnumerator rightAnswer()
    {
        yield return new WaitForSeconds(3);
        if (causeOfDeath.deathType == 1)
        {
            rightorwrongtext.text = "<align=center><size=34><#38e00d>You saved an inocent man from going to prison for life! \nPress R to restart";
        }
        else if (causeOfDeath.deathType == 2)
        {
            rightorwrongtext.text = "<align=center><size=34><#38e00d>You helped put away a murderer for life! \nPress R to restart";
        }
    }



}

//Code Generated using G4P GUI builder
//Buttons and windows designed in the builder
//Code the generated here
//I added in functions and function calls to use more windows

//HOME SCREEN "PICK YOUR STATEMENT" BUTTON
public void button5_click1(GButton source, GEvent event) { //_CODE_:button5:409785:
  println("button5 - GButton >> GEvent." + event + " @ " + millis());
  window7();
} //_CODE_:button5:409785:

//HOME SCREEN "SPIN DOCTOR" BUTTON
public void button6_click1(GButton source, GEvent event) { //_CODE_:button6:214163:
  println("button6 - GButton >> GEvent." + event + " @ " + millis());
  window4();
} //_CODE_:button6:214163:

//HOME SCREEN HYPE DOCTOR BUTTON
public void button7_click1(GButton source, GEvent event) { //_CODE_:button7:522209:
  println("button7 - GButton >> GEvent." + event + " @ " + millis());
  window8();
} //_CODE_:button7:522209:

//WHAT ARE YOU OUTRAGED ABOUT?
synchronized public void win_draw1(PApplet appc, GWinData data) { //_CODE_:window1:617279:
  appc.background(0);
} //_CODE_:window1:617279:

//CYCLISTS 
public void button9_click1(GButton source, GEvent event) { //_CODE_:button9:586050:
  println("button9 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(0,3));
  topic(decider);
  window2();
  window1.setVisible(false);
} //_CODE_:button9:586050:

//COVID
public void button10_click1(GButton source, GEvent event) { //_CODE_:button10:829939:
  println("button10 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(3,6));
  topic(decider);
  window2();
  window1.setVisible(false);
} //_CODE_:button10:829939:

//THE GOVERNMENT
public void button11_click1(GButton source, GEvent event) { //_CODE_:button11:535055:
  println("button11 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(6,9));
  topic(decider);
  window2();
  window1.setVisible(false);
} //_CODE_:button11:535055:

//SOCIETY
public void button12_click1(GButton source, GEvent event) { //_CODE_:button12:589884:
  println("button12 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(9,12));
  topic(decider);
  window2();
  window1.setVisible(false);
} //_CODE_:button12:589884:

//WHAT ABOUT IT WINDOW?
synchronized public void win_draw2(PApplet appc, GWinData data) { //_CODE_:window2:341626:
  appc.background(0);
} //_CODE_:window2:341626:

//IT'S BORING
public void button13_click1(GButton source, GEvent event) { //_CODE_:button13:468155:
  println("button13 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(0,3));
  feelings(decider);
  window3();
  window2.setVisible(false);
} //_CODE_:button13:468155:

//PEOPLE WON'T SHUT UP ABOUT IT
public void button14_click1(GButton source, GEvent event) { //_CODE_:button14:350029:
  println("button14 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(3,6));
  feelings(decider);
  window3();
  window2.setVisible(false);
} //_CODE_:button14:350029:

//IT'S ANNOYING
public void button15_click1(GButton source, GEvent event) { //_CODE_:button15:295954:
  println("button15 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(6,9));
  feelings(decider);
  window3();
  window2.setVisible(false);
} //_CODE_:button15:295954:

//IT'S FUCKING BULLSHIT
public void button16_click1(GButton source, GEvent event) { //_CODE_:button16:930071:
  println("button16 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(9,12));
  feelings(decider);
  window3();
  window2.setVisible(false);
} //_CODE_:button16:930071:

//STATEMENT CONSTRUCTOR WINDOW
synchronized public void win_draw3(PApplet appc, GWinData data) { //_CODE_:window3:321245:
  appc.background(0);
} //_CODE_:window3:321245:

//GO AGAIN?
public void button17_click1(GButton source, GEvent event) { //_CODE_:button17:579109:
  println("button17 - GButton >> GEvent." + event + " @ " + millis());
  window3.setVisible(false);
} //_CODE_:button17:579109:

//POSITIVE OR NEGATIVE SPIN WINDOW?
synchronized public void win_draw4(PApplet appc, GWinData data) { //_CODE_:window4:542115:
  appc.background(0);
} //_CODE_:window4:542115:

//POSITIVE
public void button1_click1(GButton source, GEvent event) { //_CODE_:button1:371143:
  println("button1 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(0,3));
  spin(decider);
  window5();
  window4.setVisible(false);
} //_CODE_:button1:371143:

//NEGATIVE
public void button2_click1(GButton source, GEvent event) { //_CODE_:button2:727619:
  println("button2 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(3,6));
  spin(decider);
  window5();
  window4.setVisible(false);
} //_CODE_:button2:727619:

//ABOUT WINDOW?
synchronized public void win_draw5(PApplet appc, GWinData data) { //_CODE_:window5:373328:
  appc.background(0);
} //_CODE_:window5:373328:

//THE PARTY1
public void button3_click1(GButton source, GEvent event) { //_CODE_:button3:492126:
  println("button3 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(0,3));
  spinTopic(decider);
  window6();
  window5.setVisible(false);
} //_CODE_:button3:492126:

//THE ISSUE
public void button4_click1(GButton source, GEvent event) { //_CODE_:button4:970297:
  println("button4 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(3,6));
  spinTopic(decider);
  window6();
  window5.setVisible(false);
} //_CODE_:button4:970297:

//FOR WHO WINDOW
synchronized public void win_draw6(PApplet appc, GWinData data) { //_CODE_:window6:655387:
  appc.background(0);
} //_CODE_:window6:655387:

//THE PARTY2
public void button18_click1(GButton source, GEvent event) { //_CODE_:button18:860418:
  println("button18 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(0,3));
  benefits(decider);
  window3();
  window6.setVisible(false);
} //_CODE_:button18:860418:

//THE PEOPLE
public void button19_click1(GButton source, GEvent event) { //_CODE_:button19:314499:
  println("button19 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(3,6));
  benefits(decider);
  window3();
  window6.setVisible(false);
} //_CODE_:button19:314499:

//HOW OUTRAGED ARE YOU? WINDOW
synchronized public void win_draw7(PApplet appc, GWinData data) { //_CODE_:window7:451506:
  appc.background(0);
} //_CODE_:window7:451506:

//NOT VERY
public void button20_click1(GButton source, GEvent event) { //_CODE_:button20:491340:
  println("button20 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(0,3));
  outrageLevel(decider);
  window1();
  window7.setVisible(false);
} //_CODE_:button20:491340:

//A LIL BIT
public void button21_click1(GButton source, GEvent event) { //_CODE_:button21:400113:
  println("button21 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(3,6));
  outrageLevel(decider);
  window1();
  window7.setVisible(false);
} //_CODE_:button21:400113:

//MILDLY
public void button22_click1(GButton source, GEvent event) { //_CODE_:button22:489871:
  println("button22 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(6,9));
  outrageLevel(decider);
  window1();
  window7.setVisible(false);
} //_CODE_:button22:489871:

//VERY
public void button23_click1(GButton source, GEvent event) { //_CODE_:button23:797720:
  println("button23 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(9,12));
  outrageLevel(decider);
  window1();
  window7.setVisible(false);
} //_CODE_:button23:797720:

//HOW HYPED ARE YOU? WINDOW
synchronized public void win_draw8(PApplet appc, GWinData data) { //_CODE_:window8:666734:
  appc.background(0);
} //_CODE_:window8:666734:

//SLIGHTLY
public void button24_click1(GButton source, GEvent event) { //_CODE_:button24:438235:
  println("button24 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(0,3));
  hypeLevel(decider);
  window9();
  window8.setVisible(false);
} //_CODE_:button24:438235:

//VERY
public void button25_click1(GButton source, GEvent event) { //_CODE_:button25:580190:
  println("button25 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(3,6));
  hypeLevel(decider);
  window9();
  window8.setVisible(false);
} //_CODE_:button25:580190:

//FOR WHAT? WINDOW
synchronized public void win_draw9(PApplet appc, GWinData data) { //_CODE_:window9:929721:
  appc.background(0);
} //_CODE_:window9:929721:

//MUSIC
public void button26_click1(GButton source, GEvent event) { //_CODE_:button26:310356:
  println("button26 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(0,3));
  hypeMedium(decider);
  window10();
  window9.setVisible(false);
} //_CODE_:button26:310356:

//ARTWORK
public void button27_click1(GButton source, GEvent event) { //_CODE_:button27:793979:
  println("button27 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(3,6));
  hypeMedium(decider);
  window10();
  window9.setVisible(false);
} //_CODE_:button27:793979:

//PRODUCT
public void button28_click1(GButton source, GEvent event) { //_CODE_:button28:217610:
  println("button28 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(6,9));
  hypeMedium(decider);
  window10();
  window9.setVisible(false);
} //_CODE_:button28:217610:

//EVENT
public void button29_click1(GButton source, GEvent event) { //_CODE_:button29:350574:
  println("button29 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(9,12));
  hypeMedium(decider);
  window10();
  window9.setVisible(false);
} //_CODE_:button29:350574:

//HOW MYSTERIOUS DO YOU WANT TO BE ? WINDOW
synchronized public void win_draw10(PApplet appc, GWinData data) { //_CODE_:window10:422241:
  appc.background(0);
} //_CODE_:window10:422241:

//NOT AT ALL
public void button30_click1(GButton source, GEvent event) { //_CODE_:button30:592599:
  println("button30 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(0,3));
  mystery(decider);
  window3();
  window10.setVisible(false);
} //_CODE_:button30:592599:

//VERY
public void button31_click1(GButton source, GEvent event) { //_CODE_:button31:415027:
  println("button31 - GButton >> GEvent." + event + " @ " + millis());
  decider = int(random(3,6));
  mystery(decider);
  window3();
  window10.setVisible(false);
} //_CODE_:button31:415027:


//Here's the code for the home screen
public void createGUI(){
  G4P.messagesEnabled(false);
  G4P.setGlobalColorScheme(GCScheme.BLUE_SCHEME);
  G4P.setCursor(ARROW);
  surface.setTitle("Pick Your Statement");
  label1 = new GLabel(this, 216, 1, 164, 65);
  label1.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label1.setText("Welcome To Pick Your Statement");
  label1.setTextBold();
  label1.setOpaque(true);
  label2 = new GLabel(this, 216, 111, 160, 76);
  label2.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label2.setText("What Statement Do You Need ?");
  label2.setTextBold();
  label2.setOpaque(true);
  button5 = new GButton(this, 100, 300, 80, 30);
  button5.setText("Pick Your Outrage");
  button5.setTextBold();
  button5.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  button5.addEventHandler(this, "button5_click1");
  button6 = new GButton(this, 250, 300, 80, 30);
  button6.setText("Spin Doctor");
  button6.setTextBold();
  button6.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  button6.addEventHandler(this, "button6_click1");
  button7 = new GButton(this, 400, 300, 80, 30);
  button7.setText("Hype Doctor");
  button7.setTextBold();
  button7.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  button7.addEventHandler(this, "button7_click1");
}


//From here on are functions for each window
//Functions allow me to open & close each one
void window1(){
  window1 = GWindow.getWindow(this, "Window title", 0, 0, 600, 600, JAVA2D);
  window1.noLoop();
  window1.addDrawHandler(this, "win_draw3");
  label3 = new GLabel(window1, 216, 111, 160, 76);
  label3.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label3.setText("What Are You Outraged About?");
  label3.setTextBold();
  label3.setOpaque(true);
  button9 = new GButton(window1, 100, 300, 80, 30);
  button9.setText("Cyclists");
  button9.setTextBold();
  button9.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  button9.addEventHandler(this, "button9_click1");
  button10 = new GButton(window1, 200, 300, 80, 30);
  button10.setText("Covid");
  button10.setTextBold();
  button10.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  button10.addEventHandler(this, "button10_click1");
  button11 = new GButton(window1, 300, 300, 80, 30);
  button11.setText("The Government");
  button11.setTextBold();
  button11.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  button11.addEventHandler(this, "button11_click1");
  button12 = new GButton(window1, 400, 300, 80, 30);
  button12.setText("Society");
  button12.setTextBold();
  button12.setLocalColorScheme(GCScheme.RED_SCHEME);
  button12.addEventHandler(this, "button12_click1");
  window1.loop();
}

void window2(){
 window2 = GWindow.getWindow(this, "Window title", 0, 0, 600, 600, JAVA2D);
  window2.noLoop();
  window2.addDrawHandler(this, "win_draw4");
  label5 = new GLabel(window2, 216, 111, 160, 76);
  label5.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label5.setText("What about it?");
  label5.setTextBold();
  label5.setOpaque(true);
  button13 = new GButton(window2, 100, 300, 80, 30);
  button13.setText("It's Boring");
  button13.setTextBold();
  button13.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  button13.addEventHandler(this, "button13_click1");
  button14 = new GButton(window2, 200, 291, 81, 52);
  button14.setText("People won't shut up about it");
  button14.setTextBold();
  button14.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  button14.addEventHandler(this, "button14_click1");
  button15 = new GButton(window2, 300, 300, 80, 30);
  button15.setText("It's Annoying");
  button15.setTextBold();
  button15.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  button15.addEventHandler(this, "button15_click1");
  button16 = new GButton(window2, 400, 300, 80, 30);
  button16.setText("It's Fucking Bullshit");
  button16.setTextBold();
  button16.setLocalColorScheme(GCScheme.RED_SCHEME);
  button16.addEventHandler(this, "button16_click1");
  window2.loop();
}

void window3(){
  window3 = GWindow.getWindow(this, "Window title", 0, 0, 600, 600, JAVA2D);
  window3.noLoop();
  window3.addDrawHandler(this, "win_draw4");
  label4 = new GLabel(window3, 150, 150, 300, 300);
  label4.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label4.setText(start + middle + end);
  label4.setTextBold();
  label4.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  label4.setOpaque(true);
  button17 = new GButton(window3, 260, 500, 80, 30);
  button17.setText("Go Again?");
  button17.setTextBold();
  button17.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  button17.addEventHandler(this, "button17_click1");
  window3.loop();
}

void window4(){
   window4 = GWindow.getWindow(this, "Window title", 0, 0, 600, 600, JAVA2D);
  window4.noLoop();
  window4.addDrawHandler(this, "win_draw1");
  label6 = new GLabel(window4, 199, 110, 201, 121);
  label6.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label6.setText("Positive or negative Spin?");
  label6.setTextBold();
  label6.setOpaque(true);
  button1 = new GButton(window4, 100, 300, 80, 30);
  button1.setText("Positive");
  button1.setTextBold();
  button1.addEventHandler(this, "button1_click1");
  button2 = new GButton(window4, 400, 300, 80, 30);
  button2.setText("Negative");
  button2.setTextBold();
  button2.setLocalColorScheme(GCScheme.RED_SCHEME);
  button2.addEventHandler(this, "button2_click1");
  window4.loop();
}

void window5(){
  window5 = GWindow.getWindow(this, "Window title", 0, 0, 600, 600, JAVA2D);
  window5.noLoop();
  window5.addDrawHandler(this, "win_draw2");
  label7 = new GLabel(window5, 200, 110, 201, 121);
  label7.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label7.setText("About?");
  label7.setTextBold();
  label7.setOpaque(true);
  button3 = new GButton(window5, 100, 300, 80, 30);
  button3.setText("The Party");
  button3.setTextBold();
  button3.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  button3.addEventHandler(this, "button3_click1");
  button4 = new GButton(window5, 400, 301, 80, 30);
  button4.setText("The Issue");
  button4.setTextBold();
  button4.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  button4.addEventHandler(this, "button4_click1");
  window5.loop();
}

void window6(){
  window6 = GWindow.getWindow(this, "Window title", 0, 0, 600, 600, JAVA2D);
  window6.noLoop();
  window6.addDrawHandler(this, "win_draw2");
  label8 = new GLabel(window6, 200, 110, 200, 120);
  label8.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label8.setText("For Who?");
  label8.setTextBold();
  label8.setOpaque(true);
  button18 = new GButton(window6, 100, 300, 80, 30);
  button18.setText("The Party");
  button18.setTextBold();
  button18.addEventHandler(this, "button18_click1");
  button19 = new GButton(window6, 400, 300, 80, 30);
  button19.setText("The People");
  button19.setTextBold();
  button19.setLocalColorScheme(GCScheme.RED_SCHEME);
  button19.addEventHandler(this, "button19_click1");
  window6.loop();
}

void window7(){
 window7 = GWindow.getWindow(this, "Window title", 0, 0, 600, 600, JAVA2D);
  window7.noLoop();
  window7.addDrawHandler(this, "win_draw3");
  label9 = new GLabel(window7, 216, 120, 160, 76);
  label9.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label9.setText("How Outraged Are You?");
  label9.setTextBold();
  label9.setOpaque(true);
  button20 = new GButton(window7, 100, 300, 80, 30);
  button20.setText("Not Very");
  button20.setTextBold();
  button20.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  button20.addEventHandler(this, "button20_click1");
  button21 = new GButton(window7, 200, 300, 80, 30);
  button21.setText("A lil Bit");
  button21.setTextBold();
  button21.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  button21.addEventHandler(this, "button21_click1");
  button22 = new GButton(window7, 300, 300, 80, 30);
  button22.setText("Mildly");
  button22.setTextBold();
  button22.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  button22.addEventHandler(this, "button22_click1");
  button23 = new GButton(window7, 400, 301, 80, 30);
  button23.setText("Very");
  button23.setTextBold();
  button23.setLocalColorScheme(GCScheme.RED_SCHEME);
  button23.addEventHandler(this, "button23_click1");
  window7.loop();
}

void window8(){
  window8 = GWindow.getWindow(this, "Window title", 0, 0, 600, 600, JAVA2D);
  window8.noLoop();
  window8.addDrawHandler(this, "win_draw4");
  label10 = new GLabel(window8, 200, 110, 200, 120);
  label10.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label10.setText("How Hyped Are You?");
  label10.setTextBold();
  label10.setOpaque(true);
  button24 = new GButton(window8, 100, 300, 80, 30);
  button24.setText("Slightly");
  button24.setTextBold();
  button24.addEventHandler(this, "button24_click1");
  button25 = new GButton(window8, 400, 300, 80, 30);
  button25.setText("Very");
  button25.setTextBold();
  button25.setLocalColorScheme(GCScheme.RED_SCHEME);
  button25.addEventHandler(this, "button25_click1");
  window8.loop();
}

void window9(){
 window9 = GWindow.getWindow(this, "Window title", 0, 0, 600, 600, JAVA2D);
  window9.noLoop();
  window9.addDrawHandler(this, "win_draw5");
  label11 = new GLabel(window9, 200, 100, 200, 120);
  label11.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label11.setText("For What?");
  label11.setTextBold();
  label11.setOpaque(true);
  button26 = new GButton(window9, 100, 300, 80, 30);
  button26.setText("Music");
  button26.setTextBold();
  button26.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  button26.addEventHandler(this, "button26_click1");
  button27 = new GButton(window9, 200, 300, 80, 30);
  button27.setText("Artwork");
  button27.setTextBold();
  button27.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  button27.addEventHandler(this, "button27_click1");
  button28 = new GButton(window9, 300, 300, 80, 30);
  button28.setText("Product");
  button28.setTextBold();
  button28.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  button28.addEventHandler(this, "button28_click1");
  button29 = new GButton(window9, 400, 300, 80, 30);
  button29.setText("Event");
  button29.setTextBold();
  button29.setLocalColorScheme(GCScheme.RED_SCHEME);
  button29.addEventHandler(this, "button29_click1");
  window9.loop();
}

void window10(){
 window10 = GWindow.getWindow(this, "Window title", 0, 0, 600, 600, JAVA2D);
  window10.noLoop();
  window10.addDrawHandler(this, "win_draw6");
  label12 = new GLabel(window10, 200, 100, 200, 120);
  label12.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  label12.setText("How Mysterious do you want to be?");
  label12.setTextBold();
  label12.setOpaque(true);
  button30 = new GButton(window10, 100, 300, 80, 30);
  button30.setText("Not At All");
  button30.setTextBold();
  button30.addEventHandler(this, "button30_click1");
  button31 = new GButton(window10, 400, 300, 80, 30);
  button31.setText("Very");
  button31.setTextBold();
  button31.setLocalColorScheme(GCScheme.RED_SCHEME);
  button31.addEventHandler(this, "button31_click1");
  window10.loop();
}


// Variables
GLabel label1; 
GLabel label2; 
GButton button5; 
GButton button6; 
GButton button7; 
GWindow window1;
GLabel label3; 
GButton button9; 
GButton button10; 
GButton button11; 
GButton button12; 
GWindow window2;
GLabel label5; 
GButton button13; 
GButton button14; 
GButton button15; 
GButton button16; 
GWindow window3;
GLabel label4; 
GButton button17; 
GWindow window4;
GLabel label6; 
GButton button1; 
GButton button2; 
GWindow window5;
GLabel label7; 
GButton button3; 
GButton button4; 
GWindow window6;
GLabel label8; 
GButton button18; 
GButton button19; 
GWindow window7;
GLabel label9; 
GButton button20; 
GButton button21; 
GButton button22; 
GButton button23; 
GWindow window8;
GLabel label10; 
GButton button24; 
GButton button25; 
GWindow window9;
GLabel label11; 
GButton button26; 
GButton button27; 
GButton button28; 
GButton button29; 
GWindow window10;
GLabel label12; 
GButton button30; 
GButton button31; 

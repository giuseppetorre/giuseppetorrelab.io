//Pick Your Statement
//Eoin O'Sullivan
//Hit run and build your generic statement to share online


import g4p_controls.*;

String start, middle, end;
int decider;

void setup(){
  size(600, 600);
  background(0);
  createGUI(); 
}

void draw(){ 
}


/*Each function below controls a section of statement
  Each statement consists of:
    Start
    Middle
    End
  Put them together and you get your statement
  The variables sent to each function comes
  from random number generators in the GUI tab
  They're triggered when buttons are pressed
*/


//Next 3 functions decide start, middle and end for outrage
void outrageLevel(int level){
  switch(level){
    case 0:
    start = "I don't think it matters that ";
    break;
    case 1:
    start = "Its not that important that ";
    break;
    case 2:
    start = "It's quite irrelevent that ";
    break;
    case 3:
    start = "Who cares that ";
    break;
    case 4:
    start = "As if I care that ";
    break;
    case 5:
    start = "Why would you be bothered thinking about ";
    break;
    case 6:
    start = "I just think it's funny that ";
    break;
    case 7:
    start = "Wow, that says it all that ";
    break;
    case 8:
    start = "Very convenient that ";
    break;
    case 9:
    start = "It's a disgrace that ";
    break;
    case 10:
    start = "It's such a joke that ";
    break;
    case 11:
    start = "I cannot beleive that ";
    break;
  }
}

void topic(int topicNum){
  
  switch(topicNum){
    case 0:
    middle = "cyclists are on the roads, ";
    break;
    case 1:
    middle = "everyone is on their bikes, ";
    break;
    case 2:
    middle = "people are on their bikes are everywhere, ";
    break;
    case 3:
    middle = "covid is spreading around the community, ";
    break;
    case 4:
    middle = "this bloody pandemic is endless, ";
    break;
    case 5:
    middle = "we are losing a year of our lives, ";
    break;
    case 6:
    middle = "the government we elected, ";
    break;
    case 7:
    middle = "those jokers are up in Leinster House, ";
    break;
    case 8:
    middle = "Tds are giving themselves a pay increase, ";
    break;
    case 9:
    middle = "society is getting so messed up ";
    break;
    case 10:
    middle = "we are destroying the planet, ";
    break;
    case 11:
    middle = "neoliberalism is destroying my soul, ";
    break;
  }
}

void feelings(int feels){
  switch(feels){
    case 0:
    end = "it's getting quite boring. ";
    break;
    case 1:
    end = "I could take it or leave it tbh. ";
    break;
    case 2:
    end = "I don't know who has time for it. ";
    break;
    case 3:
    end = "people won't shut up about it. ";
    break;
    case 4:
    end = "seems like it's all anyone wants to talk about. ";
    break;
    case 5:
    end = "I can't seem to escape it ";
    break;
    case 6:
    end = "it's starting to annoy me. ";
    break;
    case 7:
    end = "it's so painful to listen to. ";
    break;
    case 8:
    end = "I can't take any more of it. ";
    break;
    case 9:
    end = "what a load of fucking bullshit! ";
    break;
    case 10:
    end = "makes me want to go live on Mars! ";
    break;
    case 11:
    end = "THE WORLD IS BURNING! ";
    break;
  }
}

//Next 3 functions decide start, middle and end for spin
void spin(int spinVal){
  switch(spinVal){
    case 0:
    start = "Its a great time to acknowledge that ";
    break;
    case 1:
    start = "I'm extremely grateful that ";
    break;
    case 2:
    start = "Excellent to get this update that ";
    break;
    case 3:
    start = "Is it any wonder that ";
    break;
    case 4:
    start = "Time and time again it seems that ";
    break;
    case 5:
    start = "You really have to question if ";
    break;
  }
}

void spinTopic(int topic){
  switch(topic){
    case 0:
    middle = "'insert party name' working in this manner ";
    break;
    case 1:
    middle = "the work of 'insert party name' ";
    break;
    case 2:
    middle = ",despite the circumstances, 'insert party name' once again ";
    break;
    case 3:
    middle = "the work on 'insert issue here' ";
    break;
    case 4:
    middle = "all the hard work by people on the frontline of 'insert service here'";
    break;
    case 5:
    middle = "the difficulties in acheiving 'insert issue here' ";
    break;
  }
}

void benefits(int who){
  switch(who){
    case 0:
    end = "has shown to be the way forward as a party.";
    break;
    case 1:
    end = "is beneficial to all our members.";
    break;
    case 2:
    end = "has lead to significant change in our support.";
    break;
    case 3:
    end = "will affect the citizens of 'insert constituancy here'";
    break;
    case 4:
    end = "has demonstrated the ability to influence voters' lives.";
    break;
    case 5:
    end = "has an interest amongst the traditional voter base.";
    break;
  }
  
}

//Next 3 functions decide start, middle and end for hype
void hypeLevel(int hype){
  switch(hype){
    case 0:
    start = "Pleased to announce ";
    break;
    case 1:
    start = "After months of development ";
    break;
    case 2:
    start = "Hello, ";
    break;
    case 3:
    start = "We're so so happy to share ";
    break;
    case 4:
    start = "Are you ready??? Long awaited, ";
    break;
    case 5:
    start = "You ask and we provide - ";
    break;
  }
}

void hypeMedium(int medium){
  switch(medium){
    case 0:
    middle = "our new psych-chamber/metal-fusion concept album is coming out soon, ";
    break;
    case 1:
    middle = "a musical journey, guided by visceral synths, along the danube through the eyes of James Joyce, ";
    break;
    case 2:
    middle = "non-linear experimental punk-folk, ";
    break;
    case 3:
    middle = "a cross modal exploration of what it means to 'be', ";
    break;
    case 4:
    middle = "we welcome into the world a culmination of an immersion in Mongolian throat singing, ";
    break;
    case 5:
    middle = "our singular performance of this spoken-word/word-spoken piece, ";
    break;
    case 6:
    middle = "4 pieces of chicken, no bread, only bacon, ";
    break;
    case 7:
    middle = "our newest addition to the 45-60 age categorys favourite compression sock range, ";
    break;
    case 8:
    middle = "you need to buy this shiny thing you're neighbour has already got, ";
    break;
    case 9:
    middle = "a series of performances live-streamed from inside the Spire in Dublin, ";
    break;
    case 10:
    middle = "the live, raucous, violent, grungy, visceral, exciting tones of Post-Punk band no.5, ";
    break;
    case 11:
    middle = "please come to our shop and buy all these selection boxes and slabs of beer we bought, ";
    break;
  }
}

void mystery(int howMysterious){
   switch(howMysterious){
    case 0:
    end = "20.01.2020.";
    break;
    case 1:
    end = "more info to be announced in the next 48hrs.";
    break;
    case 2:
    end = "check out the link in our bio for more information.";
    break;
    case 3:
    end = "stay tuned for more.";
    break;
    case 4:
    end = "coming soon.";
    break;
    case 5:
    end = "you know what to do.";
    break;
  } 
}
